import argparse

from utils.hparams import HParam
from utils.wav2mel import Wav2Mel


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, default='config/config.yaml',
                        help='.yaml file for configuration')
    parser.add_argument('-d', '--data_dir', type=str, required=True,
                        help='root directory of data')
    parser.add_argument('-p', '--process_num', type=int, default=None,
                        help='number of processes to run. default: cpu_count')
    parser.add_argument('-v', '--vad', type=int, default=1,
                        help='do vad (1: True / 0: False)')
    args = parser.parse_args()
    hp = HParam(args.config)

    max_sample = hp.audio.win_length + (hp.data.frame_max - 1) * hp.audio.hop_length
    frame_max = hp.data.frame_max

    wav2mel = Wav2Mel(args, hp.audio.sample_rate, hp.audio.n_fft, hp.audio.num_mels,
                        hp.audio.hop_length, hp.audio.win_length, max_sample, frame_max, args.vad)
    wav2mel.process()

if __name__ == '__main__':
    main()