import os
import glob
import tqdm
import torch
import librosa
import threading
from multiprocessing import cpu_count

from .audio import Audio


class Wav2Mel():
    def __init__(self, args, sr, n_fft, num_mels, hop_length, win_length, max_sample, frame_max, vad):
        self._args = args
        self.audio = Audio(sr, n_fft, num_mels, hop_length, win_length)
        self.wav_list = glob.glob(os.path.join(args.data_dir, '**', '*.wav'), recursive=True)
        self.sr = sr
        self.max_sample = max_sample
        self.frame_max = frame_max
        self.vad = vad
        self.cpu_num = 0

    def mypool(self, cpu):
        for i in tqdm.trange(cpu, len(self.wav_list), self.cpu_num):
            self.wav2mel(self.wav_list[i])

    def wav2mel(self, path):
        wav, sr = librosa.load(path)
        if sr != self.sr:
            wav = librosa.resample(wav, sr, self.sr)

        if self.vad:
            vad = librosa.effects.split(wav, top_db=20)
            for idx, interval in enumerate(vad):
                if interval[1] - interval[0] > self.max_sample:
                    part = wav[interval[0]:interval[1]]
                    mel = self.audio.get_mel(part)
                    assert mel.shape[-1] >= self.frame_max, \
                        'Mel time frame %d shorter than expected %d' % (mel.shape[-1], self.frame_max)
                    mel_path = os.path.splitext(path)[0] + '-vad{idx}.pt'.format(idx=idx)
                    torch.save(mel, mel_path)
        else:
            mel = self.audio.get_mel(wav)
            mel_path = os.path.splitext(path)[0] + '.pt'
            torch.save(mel, mel_path)

    def process(self):
        cpu_num = self._args.process_num
        if cpu_num is None:
            cpu_num = cpu_count()
        self.cpu_num = cpu_num
        for cpu in range(cpu_num):
            worker = threading.Thread(target=self.mypool, args=(cpu,))
            worker.start()
