import os
import math
import torch
import torch.nn as nn
import traceback

from .validation import validate
from model.model import GE2ELoss


def train(out_dir, chkpt_path, trainset, valset, writer, logger, hp, hp_str):
    model = GE2ELoss(hp).cuda()
    if hp.train.optimizer == 'adam':
        optimizer = torch.optim.Adam(model.parameters(),
                                     lr=hp.train.adam)
    else:
        raise Exception("%s optimizer not supported" % hp.train.optimizer)

    step = 0

    if chkpt_path is not None:
        logger.info("Resuming from checkpoint: %s" % chkpt_path)
        checkpoint = torch.load(chkpt_path)
        model.load_state_dict(checkpoint['model'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        step = checkpoint['step']

        if hp_str != checkpoint['hp_str']:
            logger.warning("New hparams is different from checkpoint.")
    else:
        logger.info("Starting new training run")

    try:
        while True:
            model.train()
            for batch in trainset:
                batch = batch.cuda()

                optimizer.zero_grad()
                loss = model(batch)
                loss.backward()

                # gradient clipping
                nn.utils.clip_grad_norm_(model.parameters(), hp.train.grad_clip)

                # scale gradient of W, B
                for name, param in model.named_parameters():
                    if 'simscale' in name:
                        param.grad *= hp.train.grad_scale.scale
                    if 'linear_layer' in name:
                        param.grad *= hp.train.grad_scale.proj

                optimizer.step()

                step += 1
                loss = loss.item()
                if loss > 1e8 or math.isnan(loss):
                    logger.error("Loss exploded to %.02f at step %d!" % (loss, step))
                    raise Exception("Loss exploded")

                if step % hp.train.summary_interval == 0:
                    writer.log_training(loss, step)
                    logger.info("Wrote summary at step %d" % step)

                if step % hp.train.checkpoint_interval == 0:
                    save_path = os.path.join(out_dir, 'chkpt_%d.pt' % step)
                    torch.save({
                        'model': model.state_dict(),
                        'optimizer': optimizer.state_dict(),
                        'step': step,
                        'hp_str': hp_str,
                    }, save_path)
                    logger.info("Saved checkpoint to: %s" % save_path)
                    eer, thr = validate(model, valset, writer, step)
                    logger.info("Evaluation saved at step %d | eer: %.3f | thr: %.3f "
                        % (step, eer, thr))

    except Exception as e:
        logger.info("Exiting due to exception: %s" % e)
        traceback.print_exc()
