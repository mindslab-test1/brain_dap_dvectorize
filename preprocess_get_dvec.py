import os
import glob
import tqdm
import torch
import librosa
import argparse
import datetime

import argparse

from model.model import GE2ELoss
from utils.hparams import load_hparam_str

from dap_pb2 import EmbList, EmbConfig, MelConfig

def load_checkpoint(chkpt_path):
    checkpoint = torch.load(chkpt_path, map_location='cpu')
    model = checkpoint['model']
    hp_str = checkpoint['hp_str']
    hp = load_hparam_str(hp_str)
    del checkpoint

    return hp, model

def main(args):
    hp, state_dict = load_checkpoint(args.model)
    now = datetime.datetime.now().strftime("%m_%d_%H")

    model = GE2ELoss(hp).cuda()
    model.load_state_dict(state_dict)
    model.eval()

    wavlist = glob.glob(os.path.join(args.root_dir, '**', '*.wav'), recursive=True)

    if len(wavlist) == 0:
        print('No wav file found under path %s' % args.root_dir)

    for wavpath in tqdm.tqdm(wavlist, desc='Audio sanity check'):
        wav, sampling_rate = librosa.load(wavpath, sr=None)
        assert sampling_rate == hp.audio.sample_rate, \
            'Sampling rate mismatch at %s: expected %d, got %d' \
            % (wavpath, hp.audio.sample_rate, sampling_rate)

    for wavpath in tqdm.tqdm(wavlist, desc='Pre-computing d-vectors'):
        wav, sampling_rate = librosa.load(wavpath, sr=None)
        wav = torch.from_numpy(wav).cuda()
        mel = model.embedder.wav2mel(wav)
        dvector = model.embedder.inference(mel)
        dvector = dvector.cpu().detach()

        pt_path = os.path.splitext(wavpath)[0] + '_dvec.pt'
        torch.save(dvector, pt_path)

    print('Pre-computed and saved d-vectors for %d audios under path %s' %
        (len(wavlist), args.root_dir))


if __name__ =='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, default='config/default.yaml',
                        help="Configuration yaml path")
    parser.add_argument('-m', '--model', type=str, required=True,
                        help="Checkpoint(model) path")
    parser.add_argument('-r', '--root_dir', type=str, required=True,
                        help="Root directory of audios to be pre-processed")
    args = parser.parse_args()
    main(args)
