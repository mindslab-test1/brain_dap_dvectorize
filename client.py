import grpc
import argparse
import google.protobuf.empty_pb2 as empty

from dap_pb2 import WavBinary, MelSpectrogram
from dap_pb2_grpc import DvectorizeStub


class DvectorizeClient(object):
    def __init__(self, remote='127.0.0.1:41001', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = DvectorizeStub(channel)
        self.chunk_size = chunk_size

    def get_dvec(self, mel):
        return self.stub.GetDvector(mel)

    def get_dvec_from_wav(self, wav_binary):
        if type(wav_binary) == bytes:
            wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.GetDvectorFromWav(wav_binary)

    def get_emb_config(self):
        return self.stub.GetEmbConfig(empty.Empty())

    def get_mel_config(self):
        return self.stub.GetMelConfig(empty.Empty())

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield WavBinary(bin=wav_binary[idx:idx+self.chunk_size])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Dvectorize client')
    parser.add_argument('-r', '--remote', type=str, default='127.0.0.1:41001',
                        help='grpc: ip:port')
    args = parser.parse_args()

    client = DvectorizeClient(remote=args.remote)

    dvec_config = client.get_emb_config()
    mel_config = client.get_mel_config()
    print(dvec_config)
    print(mel_config)

    # d-vector from mel-spectrogram
    mel = [0.1 for x in range(mel_config.n_mel_channels * (10 * mel_config.win_length))]
    mel = MelSpectrogram(data=mel, sample_length=160000)
    dvec_data = client.get_dvec(mel).data

    print(len(dvec_data))

    # dvector from float wav data
    with open('sample.wav', 'rb') as rf:
        dvec_data = client.get_dvec_from_wav(rf.read()).data
    print(len(dvec_data))
