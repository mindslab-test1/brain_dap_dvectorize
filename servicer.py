import torch
import librosa
import numpy as np
from scipy.io.wavfile import read

from model.model import GE2ELoss
from utils.hparams import load_hparam_str

from dap_pb2_grpc import DvectorizeServicer
from dap_pb2 import EmbList, EmbConfig, MelConfig


def load_checkpoint(chkpt_path):
    checkpoint = torch.load(chkpt_path, map_location='cpu')
    model = checkpoint['model']
    hp_str = checkpoint['hp_str']
    hp = load_hparam_str(hp_str)

    return hp, model


class WavBinaryWrapper(object):
    def __init__(self, data):
        super(WavBinaryWrapper, self).__init__()
        self.data = data
        self.position = 0

    def read(self, n=-1):
        if n == -1:
            data = self.data[self.position:]
            self.position = len(self.data)
            return data
        else:
            data = self.data[self.position:self.position+n]
            self.position += n
            return data

    def seek(self, offset, whence=0):
        if whence == 0:
            self.position = offset
        elif whence == 1:
            self.position += offset
        elif whence == 2:
            self.position = len(self.data) + offset
        else:
            raise RuntimeError('test')
        return self.position

    def tell(self):
        return self.position

    def close(self):
        pass


class DvecInference(DvectorizeServicer):
    def __init__(self, args):
        hp, state_dict = load_checkpoint(args.model)
        self.hp = hp

        torch.cuda.set_device(args.device)
        self.device = args.device
        self.model = GE2ELoss(hp).cuda()
        self.model.load_state_dict(state_dict)
        self.model.eval()

        self.emb_config = EmbConfig(
            emb_dim=hp.model.emb_dim
        )
        self.mel_config = MelConfig(
            filter_length=hp.audio.n_fft//2+1,
            hop_length=hp.audio.hop_length,
            win_length=hp.audio.win_length,
            n_mel_channels=hp.audio.num_mels,
            sampling_rate=hp.audio.sample_rate,
            mel_fmin=0,
            mel_fmax=hp.audio.sample_rate//2,
        )

        del hp
        del state_dict
        torch.cuda.empty_cache()

    def GetDvector(self, mel_input, context):
        torch.cuda.set_device(self.device)
        with torch.no_grad():
            mel_input = torch.cuda.FloatTensor([d for d in mel_input.data])
            # mel_input: [1, hp.audio.num_mels, T] cuda tensor
            mel_input = mel_input.view(self.hp.audio.num_mels, -1)
            dvector = self.model.embedder.inference(mel_input, avg=False)
            dvector = dvector.float().view(-1).cpu().detach().tolist()
            dvec_data = EmbList(data=dvector)
            return dvec_data

    def GetDvectorFromWav(self, wav_binary_iterator, context):
        torch.cuda.set_device(self.device)
        with torch.no_grad():
            wav = bytearray()
            for wav_binary in wav_binary_iterator:
                wav.extend(wav_binary.bin)
            wav = WavBinaryWrapper(wav)
            sampling_rate, wav = read(wav)

            if len(wav.shape) == 2:
                wav = wav[:, 0]

            if wav.dtype == np.int16:
                wav = wav / 32768.0
            elif wav.dtype == np.int32:
                wav = wav / 2147483648.0
            elif wav.dtype == np.uint8:
                wav = (wav - 128) / 128.0

            wav = wav.astype(np.float32)

            if sampling_rate != self.mel_config.sampling_rate:
                wav = librosa.resample(wav, sampling_rate,
                                       self.mel_config.sampling_rate)
                wav = np.clip(wav, -1.0, 1.0)

            wav = torch.from_numpy(wav)
            wav = wav.cuda()
            mel = self.model.embedder.wav2mel(wav)
            dvector = self.model.embedder.inference(mel, avg=False)
            dvector = dvector.float().view(-1).cpu().detach().tolist()
            dvec_data = EmbList(data=dvector)
            return dvec_data

    def GetEmbConfig(self, empty, context):
        return self.emb_config

    def GetMelConfig(self, empty, context):
        return self.mel_config
